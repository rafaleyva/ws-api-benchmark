#!/usr/bin/python3

# the funcking python 3

import numpy as np
import requests
import time
import multiprocessing
import os
import argparse
import logging
import logging.handlers
from functools import partial


parser = argparse.ArgumentParser()
parser.add_argument('--nworkers', '-n', dest='nworkers')
parser.add_argument('--eshost', dest='eshost')
parser.add_argument('--host', dest='host')
parser.add_argument('--batch_size', dest='batchsize')
parser.add_argument(
    '--logfile',
    dest='logfile',
    default='/var/log/WSAPI_benchmark.log')

parser.add_argument('--credentials_file', dest='data_file')

TROVE_CREDENTIALS = None
CHECK_ES = True

HEADERS = {
    'X-username':'4iq',
    'X-Token':
    'aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa',
    'X-AppToken':
    'bbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbb',
    'Content-Type':'application/json'
}

LOGGER = None


def search(host, payload):
    s = time.clock()
    response = requests.post(
        host + '/api/v1/stolenid/breach/search',
        headers=HEADERS,
        json=payload).json()

    if response['status'] != 200:
        LOGGER.error('Wrong response: %s', response)
    e = time.clock()
    return e - s


def get_ES_load(es_host, average_load):
    stop = CHECK_ES
    while stop:
        nodes_info = requests.get(es_host + '/_nodes/stats/os').json()
        system_load_average = np.empty(shape=(10))

        # compute global information from the nodes info
        for k, v in nodes_info['nodes'].items():
            LOGGER.info('ES info: %s node - load average %s',
                        k, v['os']['load_average'])
            np.append(system_load_average, v['os']['load_average'])

        LOGGER.info('ES info:  load average %s in the cluster nodes',
                    v['os']['load_average'])
        np.append(average_load, system_load_average)

        stop = CHECK_ES
        time.sleep(15)


def worker(host, batch):
    # index_list = np.random.randint(1000, size=1000)
    global LOGGER
    payload_list = ({'custom': x} for x in batch)
    average_time = np.empty(shape=len(batch))
    count = 0

    s = time.clock()
    for i in payload_list:
        average_time = np.append(average_time, search(host, i))
        count += 1
    e = time.clock()

    LOGGER.info('Worker %s finished the job', os.getpid())
    LOGGER.info(
        'Average time per request (Worker %s): %s',
        os.getpid(),
        np.average(average_time))
    LOGGER.info(
        'Worker %s time: %s (%s calls to API)',
        os.getpid(),
        e - s,
        count)


def configure_logger(filePath='/var/log/WSAPI_benchmark.log',
                     level='INFO'):
    global LOGGER
    logging.getLogger("requests").setLevel(logging.WARNING)
    logging.getLogger("urllib3").setLevel(logging.WARNING)
    LOGGER = logging.getLogger()
    LOGGER.setLevel(level)
    handler = logging.handlers.WatchedFileHandler(filePath)
    f = ('%(asctime)-15s %(processName)-8s [%(process)d]'
         ' %(threadName)s [%(thread)d] %(levelname)-8s %(message)s')
    formatter = logging.Formatter(f)
    handler.setFormatter(formatter)
    LOGGER.addHandler(handler)

    return LOGGER


def main(args):
    configure_logger(args.logfile)
    worker_partial = partial(worker, args.host)
    average_time = np.empty(shape=(10))
    trove_credentials = open(args.data_file).readlines()
    batches = np.reshape(trove_credentials, (-1, int(args.batchsize)))

    # Thread to check ES statics
    check_es_thread = multiprocessing.Process(
        target=get_ES_load, args=[args.eshost, average_time])
    check_es_thread.start()

    LOGGER.info('Benchmark started with %s Workers', int(args.nworkers))
    s = time.clock()

    p = multiprocessing.Pool(int(args.nworkers))
    p.map(worker_partial, batches)
    p.close()
    p.join()

    e = time.clock()
    LOGGER.info('All workers finished in %s time', e - s)
    check_es_thread.close()
    check_es_thread.join(10)


if __name__ == '__main__':
    args = parser.parse_args()
    main(args)
